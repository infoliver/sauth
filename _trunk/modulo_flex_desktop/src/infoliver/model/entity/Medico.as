package infoliver.model.entity{
	import flash.net.registerClassAlias;
	
	registerClassAlias("br.com.infoliver.sah.negocio.entity.Medico",Medico);
	[RemoteClass(alias="br.com.infoliver.sah.negocio.entity.Medico")]
	[Bindable]
	public class Medico{
		public var sequencial:Object;
		public var nome:String;
		public var cns:String;
		public var numeroAtendimentoMes:Object;
		public var indicadorAtivo:String;
		public var ocupacao:Ocupacao=new Ocupacao;
	}
}