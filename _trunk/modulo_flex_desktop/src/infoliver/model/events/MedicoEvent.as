package infoliver.model.events{
	import leonelcasado.com.adobe.cairngorm.control.CairngormEvent;
	
	public class MedicoEvent extends CairngormEvent{
		public static const LISTAR:String = "MedicoEvent.listarMedico";
		//**********************************************************************************
		private var idDestination:String="facade";
		//**********************************************************************************

		public function MedicoEvent(pEvent:String,pTelaResult:Object,...args){
			super(pEvent,idDestination,pTelaResult,false,false,false,args);
		}
	}	
}