package br.com.infoliver.sah.configuracao.resource;

public interface ResourceConstant {
	String senhaInicialUsuario = "senhaInicialUsuario";
	//------------------------------------------------------------------------
    String mensagemAvisoCampoObrigatorio = "mensagem.aviso.campoObrigatorio";
    String mensagemAvisoEntidadeNaoEncontrada = "mensagem.aviso.entidadeNaoEncontrada";
    String mensagemAvisoEntidadeNaoAlterada = "mensagem.aviso.entidadeNaoAlterada";
    String mensagemAvisoEntidadeNaoSalva = "mensagem.aviso.entidadeNaoSalva";
    String mensagemAvisoEntidadeNaoExcluida = "mensagem.aviso.entidadeNaoExcluida";
    //------------------------------------------------------------------------
    String mensagemInformativaRegistroGravadoComSucesso = "mensagem.informativa.registroGravadoComSucesso";
    String mensagemInformativaRegistroAlteradoComSucesso = "mensagem.informativa.registroAlteradoComSucesso";
    String mensagemInformativaRegistroExcluidoComSucesso = "mensagem.informativa.registroExcluidoComSucesso";
    //------------------------------------------------------------------------
	String mensagemErroViolacaoUnique = "mensagem.erro.violacaoUnique";
	String mensagemErroViolacaoPK = "mensagem.erro.violacaoPK";
	String mensagemErroAoConsultarContendoMaisUmRegistro = "mensagem.erro.aoConsultarContendoMaisUmRegistro";
	String mensagemErroInesperado="mensagem.erro.erroInesperado";
	String mensagemErroDadoDuplicado="mensagem.erro.dadoDuplicado";
	//------------------------------------------------------------------------	
	String mensagemAvisoLoginInvalido = "mensagem.aviso.loginInvalido";	
    String mensagemAvisoUsuarioBloqueado = "mensagem.aviso.usuarioBloqueado";
    String mensagemAvisoEmpresaBloqueada = "mensagem.aviso.empresaBloqueada";
    String mensagemAvisoPrimeiroAcessoUsuario = "mensagem.aviso.primeiroAcessoUsuario";
    String mensagemAvisoSenhaNaoAlterada = "mensagem.aviso.senhaNaoAlterada";
    String mensagemAvisoSenhaDeveSerDiferenteDaAtual = "mensagem.aviso.senhaDeveSerDiferenteDaAtual";
    String mensagemAvisoNenhumArquivoRelacionadoAoCodigo = "mensagem.aviso.nenhumArquivoRelacionadoAoCodigo";    
    //------------------------------------------------------------------------
    String mensagemErroArquivoAdulterado = "mensagem.erro.arquivoAdulterado";
    //------------------------------------------------------------------------
}