package br.com.infoliver.sah.negocio.bo;

import java.util.List;

import br.com.infoliver.sah.negocio.entity.Encaminhador;

public interface IEncaminhadorBO {
	List<Encaminhador> listar();
}