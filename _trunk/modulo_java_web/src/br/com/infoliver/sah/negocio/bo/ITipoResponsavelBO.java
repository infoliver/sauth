package br.com.infoliver.sah.negocio.bo;

import java.util.List;

import br.com.infoliver.sah.negocio.entity.TipoResponsavel;

public interface ITipoResponsavelBO {
	List<TipoResponsavel> listar();
}