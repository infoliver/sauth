package br.com.infoliver.sah.negocio.bo.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.infoliver.sah.integracao.dao.IMedicoDAO;
import br.com.infoliver.sah.negocio.bo.IMedicoBO;
import br.com.infoliver.sah.negocio.entity.Medico;

@Service("medicoBO")
public class MedicoBO implements IMedicoBO {
	@Autowired	private IMedicoDAO medicoDAO;
	
	@Override
	public List<Medico> listar() {
		return medicoDAO.listar();
	}

	@Override
	public Medico consultar(Integer sequencial) {
		return medicoDAO.consultar(sequencial);
	}
}