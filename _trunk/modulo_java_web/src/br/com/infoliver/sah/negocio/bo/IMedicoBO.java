package br.com.infoliver.sah.negocio.bo;

import java.util.List;

import br.com.infoliver.sah.negocio.entity.Medico;

public interface IMedicoBO {
	List<Medico> listar();
	
	Medico consultar(Integer sequencial);
}