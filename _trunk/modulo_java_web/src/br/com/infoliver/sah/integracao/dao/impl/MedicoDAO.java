package br.com.infoliver.sah.integracao.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.infoliver.sah.integracao.dao.IMedicoDAO;
import br.com.infoliver.sah.negocio.entity.Medico;

@Repository("medicoDAO")
@Transactional(readOnly=true)
@SuppressWarnings({"unchecked","rawtypes"})
public class MedicoDAO extends DAOBase implements IMedicoDAO {

	@Override
	public List<Medico> listar() {
		MapSqlParameterSource params = new MapSqlParameterSource();		
		params.addValue("p_in_tipo_acao",1);
		params.addValue("p_in_seq_medico",null);
		//----------------------------------------------------------
		Map out = callProcedureUsingOutResultSet("admsah001",null,"sp_medico",params,
				new SqlParameter("p_in_tipo_acao",Types.INTEGER),
				new SqlParameter("p_in_seq_medico",Types.INTEGER),
				new SqlOutParameter("p_out_cursor", Types.OTHER,
						new ParameterizedRowMapper<Medico>() {
							public Medico mapRow(ResultSet rs, int rowNum)throws SQLException {
								Medico medico = new Medico();
								medico.setSequencial(rs.getInt("seq_medico"));
								medico.setNome(rs.getString("txt_nome"));
								medico.setCns(rs.getString("txt_cns"));
								return medico;
							}
						}));
	
		return (List<Medico>) out.get("p_out_cursor");
	}

	@Override
	public Medico consultar(Integer sequencial) {
		MapSqlParameterSource params = new MapSqlParameterSource();		
		params.addValue("p_in_tipo_acao",1);
		params.addValue("p_in_seq_medico",sequencial);
		//----------------------------------------------------------
		Map out = callProcedureUsingOutResultSet("admsah001",null,"sp_medico",params,
				new SqlParameter("p_in_tipo_acao",Types.INTEGER),
				new SqlParameter("p_in_seq_medico",Types.INTEGER),
				new SqlOutParameter("p_out_cursor", Types.OTHER,
						new ParameterizedRowMapper<Medico>() {
							public Medico mapRow(ResultSet rs, int rowNum)throws SQLException {
								Medico medico = new Medico();
								medico.setSequencial(rs.getInt("seq_medico"));
								medico.setNome(rs.getString("txt_nome"));
								medico.setCns(rs.getString("txt_cns"));
								return medico;
							}
						}));
		List<Medico> lista=(List<Medico>) out.get("p_out_cursor");
		return lista.size()>0?lista.get(0):null;
	}
}