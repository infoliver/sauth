package br.com.infoliver.sah.integracao.dao;

import java.util.List;

import br.com.infoliver.sah.negocio.entity.Encaminhador;

public interface IEncaminhadorDAO {
	List<Encaminhador> listar();
}