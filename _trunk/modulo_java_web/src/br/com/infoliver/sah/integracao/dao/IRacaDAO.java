package br.com.infoliver.sah.integracao.dao;

import java.util.List;

import br.com.infoliver.sah.negocio.entity.Raca;

public interface IRacaDAO {
	List<Raca> listar();
}