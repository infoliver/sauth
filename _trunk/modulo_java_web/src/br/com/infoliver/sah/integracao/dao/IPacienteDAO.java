package br.com.infoliver.sah.integracao.dao;

import java.util.List;

import br.com.infoliver.sah.configuracao.exception.DAOException;
import br.com.infoliver.sah.negocio.entity.Paciente;
import br.com.infoliver.sah.negocio.vo.PaginacaoVO;

public interface IPacienteDAO {
	
	Integer inserir(Paciente paciente)throws DAOException;
	
	void alterar(Paciente paciente)throws DAOException;

	void excluir(Paciente paciente)throws DAOException;
	
	Integer totalRegitrosParaPaginacao(PaginacaoVO paciente);

	List<Paciente> listarPaginado(PaginacaoVO paciente);

	List<Paciente> listar();
}