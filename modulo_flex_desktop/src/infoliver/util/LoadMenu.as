package infoliver.util{
	
	public class LoadMenu{
		public static const MN_CAD_PACIENTE_GERENCIADOR:uint=1;
		public static const MN_CAD_GRUPO_LAUDO:uint=2;		
		//-----------------------------------------------
		public static const MN_REL_PROCEDIMENTO_MEDICO:uint=3;
		//-----------------------------------------------
		public static const MN_MAN_EMPRESA:uint=4;		
		public static const MN_MAN_USUARIO:uint=5;		
		public static const MN_MAN_GRUPO_USUARIO:uint=6;		
		public static const MN_MAN_GRUPO_PERMISSAO:uint=7;		
		public static const MN_MAN_LOG_ACESSO:uint=8;		
		//-----------------------------------------------
		public static const MN_MAN_CONTATO:uint=9;		
		
		// PRATA
		public static const MN_CAD_MEDICO:uint=10;
		public static const MN_CAD_FERIADO:uint=11;
		public static const MN_CAD_AGENDAMENTO:uint=12;
		public static const MN_CAD_LICENCA:uint=13;
	}
}