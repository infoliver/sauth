/* TODO:
 * Adicionar constraints para
 * - cpf (unique)
 * - rg e orgaoExpedidor (unique)
 * - email (unique) ???
 */

ALTER TABLE admsah001.sah_medico
ADD txt_cpf character varying(11);

ALTER TABLE admsah001.sah_medico
ADD CONSTRAINT ak_medico_02 UNIQUE (txt_cpf);

ALTER TABLE admsah001.sah_medico
ADD txt_rg character varying (20);

ALTER TABLE admsah001.sah_medico
ADD txt_orgao_emissor character varying(4);

ALTER TABLE admsah001.sah_medico
ADD CONSTRAINT ak_medico_03 UNIQUE (txt_rg, txt_orgao_emissor, txt_uf_orgao_emissor, dat_expedicao);

ALTER TABLE admsah001.sah_medico
ADD txt_telefone1 character varying (30);

ALTER TABLE admsah001.sah_medico
ADD txt_telefone2 character varying (30);

ALTER TABLE admsah001.sah_medico
ADD txt_telefone3 character varying (30);

ALTER TABLE admsah001.sah_medico
ADD txt_email character varying (30);

ALTER TABLE admsah001.sah_medico
ADD txt_uf_orgao_emissor character varying(2);

ALTER TABLE admsah001.sah_medico
ADD dat_expedicao date;