CREATE TABLE admsah001.sah_feriado
(
	seq_feriado serial not null,
	txt_descricao character varying(55) not null,
	dth_inicio date not null,
	dth_fim date,
	txt_sempre_na_mesma_data character varying(1) DEFAULT 'S'::character varying,
	CONSTRAINT pk_feriado primary key (seq_feriado) 
);