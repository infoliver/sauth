CREATE TABLE admsah001.sah_agendamento
(
	seq_agendamento serial not null,
	dth_agendamento date not null,
	txt_turno character varying(1) DEFAULT 'S'::character varying,
	txt_situacao character varying(25) DEFAULT 'AGENDADO'::character varying,
	seq_medico integer not null,
	seq_paciente integer not null,
	CONSTRAINT pk_agendamento primary key (seq_agendamento),
	CONSTRAINT fk_agendamento_medico FOREIGN KEY (seq_medico)
      REFERENCES admsah001.sah_medico (seq_medico) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
    CONSTRAINT fk_agendamento_paciente FOREIGN KEY (seq_paciente)
      REFERENCES admsah001.sah_paciente (seq_paciente) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
);

ALTER TABLE admsah001.sah_agendamento
ADD seq_reagendamento integer null;

ALTER TABLE admsah001.sah_agendamento
ADD CONSTRAINT fk_reagendamento FOREIGN KEY (seq_reagendamento)
  REFERENCES admsah001.sah_agendamento (seq_agendamento) MATCH SIMPLE
  ON UPDATE RESTRICT ON DELETE RESTRICT;