CREATE TABLE admsah001.sah_licenca
(
	seq_licenca serial not null,
	dth_inicio date not null,
	dth_fim date,
	txt_observacao character varying(55) not null,
	seq_motivo integer not null,
	seq_medico integer not null,
	CONSTRAINT pk_licenca primary key (seq_licenca),
	CONSTRAINT fk_licenca_motivo FOREIGN KEY (seq_motivo)
      REFERENCES admsah001.sah_motivo (seq_motivo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
	CONSTRAINT fk_licenca_medico FOREIGN KEY (seq_medico)
      REFERENCES admsah001.sah_medico (seq_medico) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
);

--ALTER TABLE admsah001.sah_licenca
--ADD CONSTRAINT ak_licenca_01 UNIQUE (dth_inicio, dth_fim, seq_medico);