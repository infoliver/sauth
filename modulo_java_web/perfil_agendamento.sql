insert into admsah001.sah_grupo (txt_descricao)
values ('AGENDAMENTO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('AGENDAR', 'ROLE_INSERIR_AGENDAMENTO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('ALTERAR AGENDAMENTO', 'ROLE_ALTERAR_AGENDAMENTO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('LISTAR AGENDAMENTOS', 'ROLE_LISTAR_AGENDAMENTO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('REAGENDAR', 'ROLE_REAGENDAR');

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'AGENDAMENTO'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_INSERIR_AGENDAMENTO')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'AGENDAMENTO'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_ALTERAR_AGENDAMENTO')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'AGENDAMENTO'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_LISTAR_AGENDAMENTO')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'AGENDAMENTO'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_REAGENDAR')
);