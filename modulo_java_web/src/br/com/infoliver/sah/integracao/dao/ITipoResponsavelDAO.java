package br.com.infoliver.sah.integracao.dao;

import java.util.List;

import br.com.infoliver.sah.negocio.entity.TipoResponsavel;

public interface ITipoResponsavelDAO {
	List<TipoResponsavel> listar();
}