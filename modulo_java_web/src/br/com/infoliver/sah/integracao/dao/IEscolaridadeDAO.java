package br.com.infoliver.sah.integracao.dao;

import java.util.List;

import br.com.infoliver.sah.negocio.entity.Escolaridade;

public interface IEscolaridadeDAO {
	List<Escolaridade> listar();
}