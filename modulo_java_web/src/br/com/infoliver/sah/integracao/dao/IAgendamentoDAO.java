package br.com.infoliver.sah.integracao.dao;

import java.util.List;

import br.com.infoliver.sah.negocio.entity.Agendamento;
import br.com.infoliver.sah.negocio.entity.Horario;
import br.com.infoliver.sah.negocio.entity.Medico;
import br.com.infoliver.sah.negocio.vo.PaginacaoVO;

public interface IAgendamentoDAO {

	void alterarSituacao(Agendamento agendamento);
	
	Integer inserir(Agendamento agendamento);

	List<Agendamento> listarPaginado(PaginacaoVO agendamento);
	
	Integer totalRegitrosParaPaginacao(Agendamento entidade);
	
	public List<Horario> listarHorariosPorMedico(Medico medico);

}
