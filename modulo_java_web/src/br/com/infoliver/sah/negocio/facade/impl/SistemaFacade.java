package br.com.infoliver.sah.negocio.facade.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.flex.remoting.RemotingInclude;
import org.springframework.stereotype.Service;

import br.com.infoliver.sah.configuracao.exception.FacadeException;
import br.com.infoliver.sah.negocio.bo.IAgendamentoBO;
import br.com.infoliver.sah.negocio.bo.IArquivoBO;
import br.com.infoliver.sah.negocio.bo.IEmpresaBO;
import br.com.infoliver.sah.negocio.bo.IEncaminhadorBO;
import br.com.infoliver.sah.negocio.bo.IEscolaridadeBO;
import br.com.infoliver.sah.negocio.bo.IFeriadoBO;
import br.com.infoliver.sah.negocio.bo.IGrupoBO;
import br.com.infoliver.sah.negocio.bo.IGrupoLaudoBO;
import br.com.infoliver.sah.negocio.bo.IGrupoPermissaoBO;
import br.com.infoliver.sah.negocio.bo.IGrupoUsuarioBO;
import br.com.infoliver.sah.negocio.bo.ILicencaBO;
import br.com.infoliver.sah.negocio.bo.ILogAcessoBO;
import br.com.infoliver.sah.negocio.bo.IMedicoBO;
import br.com.infoliver.sah.negocio.bo.IMotivoBO;
import br.com.infoliver.sah.negocio.bo.IOcupacaoBO;
import br.com.infoliver.sah.negocio.bo.IPacienteBO;
import br.com.infoliver.sah.negocio.bo.IPermissaoBO;
import br.com.infoliver.sah.negocio.bo.IProcedimentoMedicoBO;
import br.com.infoliver.sah.negocio.bo.IRacaBO;
import br.com.infoliver.sah.negocio.bo.IRelatorioBO;
import br.com.infoliver.sah.negocio.bo.ITipoResponsavelBO;
import br.com.infoliver.sah.negocio.bo.IUsuarioBO;
import br.com.infoliver.sah.negocio.entity.Agendamento;
import br.com.infoliver.sah.negocio.entity.Arquivo;
import br.com.infoliver.sah.negocio.entity.Empresa;
import br.com.infoliver.sah.negocio.entity.Encaminhador;
import br.com.infoliver.sah.negocio.entity.Escolaridade;
import br.com.infoliver.sah.negocio.entity.Feriado;
import br.com.infoliver.sah.negocio.entity.Grupo;
import br.com.infoliver.sah.negocio.entity.GrupoLaudo;
import br.com.infoliver.sah.negocio.entity.GrupoLaudoPaciente;
import br.com.infoliver.sah.negocio.entity.GrupoPermissao;
import br.com.infoliver.sah.negocio.entity.GrupoUsuario;
import br.com.infoliver.sah.negocio.entity.Licenca;
import br.com.infoliver.sah.negocio.entity.Medico;
import br.com.infoliver.sah.negocio.entity.Motivo;
import br.com.infoliver.sah.negocio.entity.Ocupacao;
import br.com.infoliver.sah.negocio.entity.Paciente;
import br.com.infoliver.sah.negocio.entity.Permissao;
import br.com.infoliver.sah.negocio.entity.ProcedimentoMedico;
import br.com.infoliver.sah.negocio.entity.Raca;
import br.com.infoliver.sah.negocio.entity.Relatorio;
import br.com.infoliver.sah.negocio.entity.TipoResponsavel;
import br.com.infoliver.sah.negocio.entity.Usuario;
import br.com.infoliver.sah.negocio.facade.ISistemaFacade;
import br.com.infoliver.sah.negocio.vo.LoginVO;
import br.com.infoliver.sah.negocio.vo.PaginacaoVO;
import br.com.infoliver.sah.negocio.vo.ProcedimentoMedicoVO;
import br.com.infoliver.sah.negocio.vo.RetornoVO;

@Service("facade")
public class SistemaFacade implements ISistemaFacade {
	@Autowired private IPacienteBO pacienteBO;
	@Autowired private IArquivoBO arquivoBO;
	@Autowired private IEmpresaBO empresaBO;
	@Autowired private IEncaminhadorBO encaminhadorBO;
	@Autowired private IEscolaridadeBO escolaridadeBO;
	@Autowired private IOcupacaoBO ocupacaoBO;
	@Autowired private IRacaBO racaBO;
	@Autowired private ITipoResponsavelBO tipoResponsavelBO;
	@Autowired private IUsuarioBO usuarioBO;
	@Autowired private IPermissaoBO permissaoBO;
	@Autowired private ILogAcessoBO logAcessoBO;
	@Autowired private IGrupoUsuarioBO grupoUsuarioBO;
	@Autowired private IGrupoPermissaoBO grupoPermissaoBO;
	@Autowired private IGrupoBO grupoBO;	
	@Autowired private IGrupoLaudoBO grupoLaudoBO;	
	@Autowired private IProcedimentoMedicoBO procedimentoMedicoBO;
	@Autowired private IAgendamentoBO agendamentoBO;
	@Autowired private IFeriadoBO feriadoBO;
	@Autowired private IMedicoBO medicoBO;
	@Autowired private IRelatorioBO relatorioBO;
	@Autowired private ILicencaBO licencaBO;
	@Autowired private IMotivoBO motivoBO;
	
	//Paciente ==========================================================
	@Override
	//@Secured("ROLE_INSERIR_PACIENTE")
	public RetornoVO inserirPaciente(Paciente paciente)throws FacadeException{
		return pacienteBO.inserir(paciente);
	}
	
	@Override
	//@Secured("ROLE_ALTERAR_PACIENTE")
	public RetornoVO alterarPaciente(Paciente paciente)throws FacadeException{
		return pacienteBO.alterar(paciente);
	}

	@Override
	//@Secured("ROLE_EXCLUIR_PACIENTE")
	public RetornoVO excluirPaciente(Paciente paciente)throws FacadeException{
		return pacienteBO.excluir(paciente);
	}
	
	@Override
	//@Secured("ROLE_LISTAR_PACIENTE")
	public PaginacaoVO listarPacientePaginado(PaginacaoVO paciente){
		return pacienteBO.listarPaginado(paciente);
	}

	@Override
	//@Secured("ROLE_LISTAR_PACIENTE")
	public List<Paciente> listarPaciente(){
		return pacienteBO.listar();
	}
	@Override
	//@Secured("ROLE_LISTAR_PACIENTE")
	public List<Paciente> listarPacienteRelatorio(PaginacaoVO paciente){
		return pacienteBO.listarPacienteRelatorio(paciente);
	}
	//====================================================================

	//Arquivo ============================================================
	@Override
	//@Secured("ROLE_EXCLUIR_ARQUIVO")
	public RetornoVO excluirArquivo(Arquivo arquivo)throws FacadeException{
		return arquivoBO.excluir(arquivo);
	}

	@Override
	//@Secured("ROLE_CONSULTAR_PARA_VISUALIZACAO_ARQUIVO")
	public Arquivo consultarParaVisualizacaoArquivo(Integer sequencial){
		return arquivoBO.consultarImagem(sequencial);
	}
	
	@Override
	//@Secured("ROLE_LISTAR_ARQUIVO")
	public List<Arquivo> listarArquivo(Integer sequencialPaciente){
		return arquivoBO.listar(sequencialPaciente);
	}
	//=====================================================================

	//Empresa =============================================================
	@Override
	//@Secured("ROLE_CONSULTAR_EMPRESA")
	public Empresa consultarEmpresa(){
		return empresaBO.consultar();
	}
	
	@Override
	//@Secured("ROLE_INSERIR_EMPRESA")
	public RetornoVO inserirEmpresa(Empresa empresa)throws FacadeException{
		return empresaBO.inserir(empresa);
	}
	
	@Override
	//@Secured("ROLE_ALTERAR_EMPRESA")
	public RetornoVO alterarEmpresa(Empresa empresa)throws FacadeException{
		return empresaBO.alterar(empresa);
	}
	//======================================================================

	//Encaminhador =========================================================
	@Override
	//@Secured("ROLE_LISTAR_ENCAMINHADOR")
	public List<Encaminhador> listarEncaminhador(){
		return encaminhadorBO.listar();
	}
	//======================================================================

	//Escolaridade =========================================================
	@Override
	//@Secured("ROLE_LISTAR_ESCOLARIDADE")
	public List<Escolaridade> listarEscolaridade(){
		return escolaridadeBO.listar();
	}
	//======================================================================

	//Ocupacao =============================================================
	@Override
	//@Secured("ROLE_LISTAR_OCUPACAO")
	public List<Ocupacao> listarOcupacao(){
		return ocupacaoBO.listar();
	}
	
	@Override
	public List<Ocupacao> pesquisarOcupacaoPorDescricao(String descricao) {
		System.out.println("pesquisarOcupacaoPorDescricao called: " + descricao);
		return ocupacaoBO.pesquisarOcupacaoPorDescricao(descricao);
	}
	//======================================================================

	//Raca =================================================================
	@Override
	//@Secured("ROLE_LISTAR_RACA")
	public List<Raca> listarRaca(){
		return racaBO.listar();
	}
	//======================================================================

	//TipoResponsavel ======================================================
	@Override
	//@Secured("ROLE_LISTAR_TIPO_RESPONSAVEL")
	public List<TipoResponsavel> listarTipoResponsavel(){
		return tipoResponsavelBO.listar();
	}
	//======================================================================
	
	//Usuario ==============================================================	
	@Override
	public LoginVO acessarSistema(Usuario usuario)throws FacadeException {
		return usuarioBO.acessar(usuario);
	}
	
	@Override
	//@Secured("ROLE_INSERIR_USUARIO")
	public RetornoVO inserirUsuario(Usuario usuario) throws FacadeException {
		return usuarioBO.inserir(usuario);
	}

	@Override
	//@Secured("ROLE_ALTERAR_USUARIO")
	public RetornoVO alterarUsuario(Usuario usuario) throws FacadeException {
		return usuarioBO.alterar(usuario);
	}

	@Override
	//@Secured("ROLE_EXCLUIR_USUARIO")
	public RetornoVO excluirUsuario(Usuario usuario) throws FacadeException {
		return usuarioBO.excluir(usuario);
	}

	@Override
	//@Secured("ROLE_LISTAR_USUARIO")
	public PaginacaoVO listarUsuarioPaginado(PaginacaoVO usuario){
		System.out.println("'listaUsuarioPaginado' foi chamado!");
		return usuarioBO.listarPaginado(usuario);
	}

	@Override
	//@Secured("ROLE_LISTAR_USUARIO")
	public List<Usuario> listarUsuario(){
		return usuarioBO.listar();
	}
	
	@Override
	//@Secured("ROLE_LISTAR_USUARIO_DO_GRUPO")
	public List<Usuario> listarUsuarioDoGrupo(Integer sequencialGrupo) {
		return usuarioBO.listarUsuarioDoGrupo(sequencialGrupo);
	}

	@Override
	//@Secured("ROLE_LISTAR_USUARIO_DIFERENTE_DO_GRUPO")
	public List<Usuario> listarUsuarioDiferenteDoGrupo(Integer sequencialGrupo) {
		return usuarioBO.listarUsuarioDiferenteDoGrupo(sequencialGrupo);
	}
	//======================================================================

	//Permissao ============================================================
	@Override
	//@Secured("ROLE_LISTAR_PERMISSAO_DO_GRUPO")
	public List<Permissao> listarPermissaoDoGrupo(Integer sequencialGrupo) {
		return permissaoBO.listarPermissaoDoGrupo(sequencialGrupo);
	}

	@Override
	//@Secured("ROLE_LISTAR_PERMISSAO_DIFERENTE_DO_GRUPO")
	public List<Permissao> listarPermissaoDiferenteDoGrupo(Integer sequencialGrupo) {
		return permissaoBO.listarPermissaoDiferenteDoGrupo(sequencialGrupo);
	}
	//======================================================================

	//LogAcesso ============================================================
	@Override
	//@Secured("ROLE_LISTAR_LOG_ACESSO")
	public PaginacaoVO listarLogAcessoPaginado(PaginacaoVO logAcesso) {
		return logAcessoBO.listarPaginado(logAcesso);
	}
	//======================================================================

	//GrupoUsuario =========================================================
	@Override
	//@Secured("ROLE_INSERIR_GRUPO_USUARIO")
	public void inserirGrupoUsuario(GrupoUsuario grupoUsuario) throws FacadeException {
		grupoUsuarioBO.inserir(grupoUsuario);
	}

	@Override
	//@Secured("ROLE_EXCLUIR_GRUPO_USUARIO")
	public void excluirGrupoUsuario(GrupoUsuario grupoUsuario) throws FacadeException {
		grupoUsuarioBO.excluir(grupoUsuario);
	}
	//======================================================================
	
	//GrupoPermissao =======================================================	
	@Override
	//@Secured("ROLE_INSERIR_GRUPO_PERMISSAO")
	public void inserirGrupoPermissao(GrupoPermissao grupoPermissao) throws FacadeException {
		grupoPermissaoBO.inserir(grupoPermissao);
	}

	@Override
	//@Secured("ROLE_EXCLUIR_GRUPO_PERMISSAO")
	public void excluirGrupoPermissao(GrupoPermissao grupoPermissao) throws FacadeException {
		grupoPermissaoBO.excluir(grupoPermissao);
	}
	//======================================================================
	
	//Grupo ================================================================
	@Override
	//@Secured("ROLE_INSERIR_GRUPO")
	public void inserirGrupo(Grupo grupo) throws FacadeException {
		grupoBO.inserir(grupo);
	}

	@Override
	//@Secured("ROLE_ALTERAR_GRUPO")
	public void alterarGrupo(Grupo grupo) throws FacadeException {
		grupoBO.alterar(grupo);
	}

	@Override
	//@Secured("ROLE_EXCLUIR_GRUPO")
	public void excluirGrupo(Grupo grupo) throws FacadeException {
		grupoBO.excluir(grupo);
	}

	@Override
	//@Secured("ROLE_LISTAR_GRUPO")
	public List<Grupo> listarGrupo() {
		return grupoBO.listar();
	}
	//======================================================================

	//Grupo Laudo ==========================================================
	@Override
	//@Secured("ROLE_LISTAR_GRUPO_LAUDO")
	public List<GrupoLaudo> listarGrupoLaudo() {
		return grupoLaudoBO.listar();
	}
	
	@Override
	//@Secured("ROLE_LISTAR_PACIENTE_GRUPO_LAUDO")
	public List<Paciente> listarPacienteGrupoLaudo(GrupoLaudo grupoLaudo) {
		return grupoLaudoBO.listarPacienteGrupoLaudo(grupoLaudo);
	}
	
	@Override
	//@Secured("ROLE_INSERIR_GRUPO_LAUDO")
	public void inserirGrupoLaudo(GrupoLaudo grupoLaudo) throws FacadeException {
		grupoLaudoBO.inserir(grupoLaudo);
	}

	@Override
	//@Secured("ROLE_ALTERAR_GRUPO_LAUDO")
	public void alterarGrupoLaudo(GrupoLaudo grupoLaudo) throws FacadeException {
		grupoLaudoBO.alterar(grupoLaudo);
	}

	@Override
	//@Secured("ROLE_EXCLUIR_GRUPO_LAUDO")
	public void excluirGrupoLaudo(GrupoLaudo grupoLaudo) throws FacadeException {
		grupoLaudoBO.excluir(grupoLaudo);
	}
	
	@Override
	//@Secured("ROLE_INSERIR_GRUPO_LAUDO_PACIENTE")
	public void inserirGrupoLaudoPaciente(GrupoLaudoPaciente grupoLaudoPaciente)throws FacadeException {
		grupoLaudoBO.inserirGrupoLaudoPaciente(grupoLaudoPaciente);
	}

	@Override
	//@Secured("ROLE_EXCLUIR_GRUPO_LAUDO_PACIENTE")
	public void excluirGrupoLaudoPaciente(GrupoLaudoPaciente grupoLaudoPaciente)throws FacadeException {
		grupoLaudoBO.excluirGrupoLaudoPaciente(grupoLaudoPaciente);
	}
	//======================================================================

	//Procedimento Medico ==================================================
	@Override
	//@Secured("ROLE_LISTAR_TIPO_PROCEDIMENTO_MEDICO")
	public List<ProcedimentoMedico> listarTipoProcedimentoMedico() {
		return procedimentoMedicoBO.listarTipoProcedimentoMedico();
	}
	
	@Override
	public ProcedimentoMedicoVO consultarProcedimentoMedico(Integer seqGrupoLaudo,Integer seqProcedimentoMedico,Integer seqMedico,Integer seqRelatorio){
		return procedimentoMedicoBO.consultarProcedimentoMedico(seqGrupoLaudo,seqProcedimentoMedico,seqMedico, seqRelatorio);
	}	
	
	@Override
	//@Secured("ROLE_LISTAR_PROCEDIMENTO_MEDICO_PRINCIPAL")
	public List<ProcedimentoMedico> listarProcedimentoMedicoPrincipal() {
		return procedimentoMedicoBO.listarProcedimentoMedicoPrincipal();
	}

	@Override
	//@Secured("ROLE_LISTAR_PROCEDIMENTO_MEDICO_SECUNDARIO")
	public List<ProcedimentoMedico> listarProcedimentoMedicoSecundario() {
		return procedimentoMedicoBO.listarProcedimentoMedicoSecundario();
	}
	//======================================================================
	
	//Agendamento ===============================================================
	@Override
	public RetornoVO alterarSituacaoAgendamento(Agendamento agendamento) {
		return agendamentoBO.alterarSituacao(agendamento);
	}
	
	@Override
	public RetornoVO inserirAgendamento(Agendamento agendamento) {
		return agendamentoBO.inserir(agendamento);
	}
	
	@Override
	public PaginacaoVO listarAgendamentoPaginado(PaginacaoVO agendamento) {
		return agendamentoBO.listarPaginado(agendamento);
	}
	
	@Override
	public RetornoVO reagendarAgendamento(Agendamento agendamento) {
		return agendamentoBO.reagendarAgendamento(agendamento);
	}
	//======================================================================
	
	//Feriado ===============================================================
	@Override
	public RetornoVO alterarFeriado(Feriado feriado) {
		return feriadoBO.alterar(feriado);
	}
	
	@Override
	public RetornoVO excluirFeriado(Feriado feriado) {
		return feriadoBO.excluirFeriado(feriado);
	}
	
	@Override
	public RetornoVO inserirFeriado(Feriado feriado) {
		return feriadoBO.inserir(feriado);
	}
	
	@Override
	public List<Feriado> listarFeriado() {
		return feriadoBO.listar();
	}
	//======================================================================
	
	//Feriado ===============================================================
	@Override
	public RetornoVO alterarLicenca(Licenca licenca) {
		return licencaBO.alterar(licenca);
	}
	
	@Override
	public RetornoVO excluirLicenca(Licenca licenca) {
		return licencaBO.excluir(licenca);
	}
	
	@Override
	public RetornoVO inserirLicenca(Licenca licenca) {
		return licencaBO.inserir(licenca);
	}
	
	@Override
	public PaginacaoVO listarLicencaPaginado(PaginacaoVO licenca) {
		return licencaBO.listarPaginado(licenca);
	}
	//======================================================================

	//Medico ===============================================================
	@Override
	public RetornoVO alterarMedico(Medico medico) {
		return medicoBO.alterar(medico);
	}
	
	@Override
	public RetornoVO inserirMedico(Medico medico) {
		return medicoBO.inserir(medico);
	}
	
	@Override
	//@Secured("ROLE_LISTAR_MEDICO")
	public List<Medico> listarMedico() {
		return medicoBO.listar();
	}
	
	@Override
	public PaginacaoVO listarMedicoPaginado(PaginacaoVO medico) {
		return medicoBO.listarPaginado(medico);
	}
	//======================================================================
	
	//Motivo ===============================================================
	@Override
	public RetornoVO alterarMotivo(Motivo motivo) {
		return motivoBO.alterar(motivo);
	}
	
	@Override
	public RetornoVO excluirMotivo(Motivo motivo) {
		return motivoBO.excluir(motivo);
	}
	
	@Override
	public RetornoVO inserirMotivo(Motivo motivo) {
		return motivoBO.inserir(motivo);
	}
	
	@Override
	public List<Motivo> listarMotivo() {
		return motivoBO.listar();
	}
	//======================================================================
	
	//Relatorio ============================================================
	@Override
	//@Secured("ROLE_LISTAR_RELATORIO_PROCEDIMENTO")
	public List<Relatorio> listarRelatorioProcedimento(){
		return relatorioBO.listarRelatorioProcedimento();
	}
	//======================================================================

}