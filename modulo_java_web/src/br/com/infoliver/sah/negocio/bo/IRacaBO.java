package br.com.infoliver.sah.negocio.bo;

import java.util.List;

import br.com.infoliver.sah.negocio.entity.Raca;

public interface IRacaBO {
	List<Raca> listar();
}