package br.com.infoliver.sah.negocio.bo;

import java.util.List;

import br.com.infoliver.sah.negocio.entity.Escolaridade;

public interface IEscolaridadeBO {
	List<Escolaridade> listar();
}