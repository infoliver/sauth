create table admsah001.sah_motivo(
	seq_motivo serial not null,
	txt_descricao character varying (100) not null,
	CONSTRAINT pk_motivo primary key (seq_motivo)
);

alter table admsah001.sah_motivo
add CONSTRAINT ak_motivo_01 unique(txt_descricao);