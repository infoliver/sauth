insert into admsah001.sah_grupo (txt_descricao)
values ('RH');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('INSERIR FERIADO', 'ROLE_INSERIR_FERIADO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('ALTERAR FERIADO', 'ROLE_ALTERAR_FERIADO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('EXCLUIR FERIADO', 'ROLE_EXCLUIR_FERIADO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('LISTAR FERIADOS', 'ROLE_LISTAR_FERIADO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('INSERIR LICENÇA', 'ROLE_INSERIR_LICENCA');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('ALTERAR LICENÇA', 'ROLE_ALTERAR_LICENCA');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('EXCLUIR LICENÇA', 'ROLE_EXCLUIR_LICENCA');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('LISTAR LICENÇAS', 'ROLE_LISTAR_LICENCA');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('INSERIR MÉDICO', 'ROLE_INSERIR_MEDICO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('ALTERAR MÉDICO', 'ROLE_ALTERAR_MEDICO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('EXCLUIR MÉDICO', 'ROLE_EXCLUIR_MEDICO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('LISTAR MÉDICOS', 'ROLE_LISTAR_MEDICO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('INSERIR MOTIVO', 'ROLE_INSERIR_MOTIVO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('ALTERAR MOTIVO', 'ROLE_ALTERAR_MOTIVO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('EXCLUIR MOTIVO', 'ROLE_EXCLUIR_MOTIVO');

insert into admsah001.sah_permissao(txt_descricao, txt_chave)
values ('LISTAR MOTIVOS', 'ROLE_LISTAR_MOTIVO');

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_INSERIR_FERIADO')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_ALTERAR_FERIADO')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_EXCLUIR_FERIADO')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_LISTAR_FERIADO')
);


insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_INSERIR_MEDICO')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_INSERIR_LICENCA')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_INSERIR_LICENCA')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_ALTERAR_LICENCA')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_EXCLUIR_LICENCA')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_LISTAR_LICENCA')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_INSERIR_MEDICO')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_ALTERAR_MEDICO')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_EXCLUIR_MEDICO')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_LISTAR_MEDICO')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_INSERIR_MOTIVO')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_ALTERAR_MOTIVO')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_EXCLUIR_MOTIVO')
);

insert into admsah001.sah_grupo_permissao(seq_grupo, seq_permissao)
values (
	(select seq_grupo from admsah001.sah_grupo where txt_descricao = 'RH'),
	(select seq_permissao from admsah001.sah_permissao where txt_chave = 'ROLE_LISTAR_MOTIVO')
);