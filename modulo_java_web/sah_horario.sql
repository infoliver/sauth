CREATE TABLE admsah001.sah_horario
(
	seq_medico integer not null,
	num_dia_semana integer not null,
	txt_turno character varying(1) not null DEFAULT 'M'::character varying,
	num_maximo_agendamentos integer not null DEFAULT 0,
	CONSTRAINT fk_horario_medico FOREIGN KEY (seq_medico)
      REFERENCES admsah001.sah_medico (seq_medico) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
);

ALTER TABLE admsah001.sah_horario
ADD CONSTRAINT ak_horario_01 UNIQUE (num_dia_semana, txt_turno, seq_medico);